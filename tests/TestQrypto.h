/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QObject>

class TestQrypto : public QObject
{
  Q_OBJECT
public:
  explicit TestQrypto(QObject* parent = 0);
private slots:
  void testDsaSign();
  void testDsaSerial();
  void testRsaSign();
  void testRsaCrypt();
  void testRsaSerial();
  void testBlowCrypt();
};