/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "TestQrypto.h"

#include <QtTest/QtTest>

#include <Qrypto/DSAlgorithm.h>
#include <Qrypto/RSAAlgorithm.h>
#include <Qrypto/BlowfishAlgorithm.h>

TestQrypto::TestQrypto(QObject* parent): QObject(parent)
{

}

void TestQrypto::testDsaSign()
{
  Qrypto::DSAlgorithm al1;
  Qrypto::DSAlgorithm al2;
  al1.generateKey();
  al2.generateKey();
  
  QByteArray data1 = "Stuff to sign that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray sign1_1 = al1.sign(data1);
  QByteArray sign1_2 = al2.sign(data1);
  
  QVERIFY(sign1_1.size() > 0);
  QVERIFY(sign1_2.size() > 0);
  
  QVERIFY(al1.validate(data1, sign1_1));
  QVERIFY(al2.validate(data1, sign1_2));
  QVERIFY(not al2.validate(data1, sign1_1));
  QVERIFY(not al1.validate(data1, sign1_2));

  QByteArray data2 = "Stuff to sign that is very long and should allow to test for too long data right! But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";

  QByteArray sign2_1 = al1.sign(data2);
  QByteArray sign2_2 = al2.sign(data2);
  
  QVERIFY(sign1_1 != sign2_1);
  QVERIFY(sign1_2 != sign2_2);
}

void TestQrypto::testDsaSerial()
{
  Qrypto::DSAlgorithm al1;
  al1.generateKey();
  QByteArray serial = al1.serialise();
  Qrypto::DSAlgorithm al2;
  al2.unserialise(serial);
  serial = al1.serialise(Qrypto::AbstractSerialisable::Key::Public);
  Qrypto::DSAlgorithm al3;
  al3.unserialise(serial);
  

  QByteArray data = "Stuff to encrypt that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray enc1 = al1.sign(data);
  QByteArray enc2 = al2.sign(data);
  QVERIFY(al2.validate(data, enc1));
  QVERIFY(al1.validate(data, enc2));
  QVERIFY(al3.validate(data, enc1));
  QVERIFY(al3.validate(data, enc2));

}

void TestQrypto::testRsaSign()
{
  Qrypto::RSAAlgorithm al1;
  Qrypto::RSAAlgorithm al2;
  al1.generateKey();
  al2.generateKey();
  
  QByteArray data = "Stuff to sign that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray sign1_1 = al1.sign(data);
  QByteArray sign1_2 = al2.sign(data);

  QVERIFY(al1.validate(data, sign1_1));
  QVERIFY(al2.validate(data, sign1_2));
  QVERIFY(not al2.validate(data, sign1_1));
  QVERIFY(not al1.validate(data, sign1_2));

  QByteArray data2 = "Stuff to sign that is very long and should allow to test for too long data right! But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";

  QByteArray sign2_1 = al1.sign(data2);
  QByteArray sign2_2 = al2.sign(data2);
  
  QVERIFY(sign1_1 != sign2_1);
  QVERIFY(sign1_2 != sign2_2);
}

void TestQrypto::testRsaCrypt()
{
  Qrypto::RSAAlgorithm al1;
  Qrypto::RSAAlgorithm al2;
  al1.generateKey();
  al2.generateKey();
  
  QByteArray data = "Stuff to encrypt that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray enc1 = al1.encrypt(data);
  QByteArray enc2 = al2.encrypt(data);
  QVERIFY(enc1 != enc2);

  QCOMPARE(al1.decrypt(enc1), data);
  QCOMPARE(al2.decrypt(enc2), data);
  QVERIFY(al2.decrypt(enc1) != data);
  QVERIFY(al1.decrypt(enc2) != data);

}

void TestQrypto::testRsaSerial()
{
  Qrypto::RSAAlgorithm al1;
  al1.generateKey();
  QByteArray serial = al1.serialise();
  Qrypto::RSAAlgorithm al2;
  al2.unserialise(serial);
  serial = al1.serialise(Qrypto::AbstractSerialisable::Key::Public);
  Qrypto::RSAAlgorithm al3;
  al3.unserialise(serial);
  

  QByteArray data = "Stuff to encrypt that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray enc1 = al1.encrypt(data);
  QByteArray enc2 = al2.encrypt(data);
  QByteArray enc3 = al3.encrypt(data);
  QCOMPARE(al2.decrypt(enc1), data);
  QCOMPARE(al1.decrypt(enc2), data);
  QCOMPARE(al1.decrypt(enc3), data);
  QCOMPARE(al2.decrypt(enc3), data);
  QCOMPARE(al3.decrypt(enc1), QByteArray());
  QCOMPARE(al3.decrypt(enc2), QByteArray());
  QCOMPARE(al3.decrypt(enc3), QByteArray());  
}

void TestQrypto::testBlowCrypt()
{
  Qrypto::BlowfishAlgorithm al1;
  Qrypto::BlowfishAlgorithm al2;
  al1.setKey("This is a key that will rule the world!");
  al2.setKey("This is an other key that will rule an other world!");
  
  QByteArray data = "Stuff to encrypt that is very long and should allow to test for too long data right? But for this algorithm I need really long data that is very very very long so I will need to have a very long string since it has a very large buffer, is that enough?";
  QByteArray enc1 = al1.encrypt(data);
  QByteArray enc2 = al2.encrypt(data);
  QVERIFY(enc1 != enc2);

  QCOMPARE(al1.decrypt(enc1), data);
  QCOMPARE(al2.decrypt(enc2), data);
  QVERIFY(al2.decrypt(enc1) != data);
  QVERIFY(al1.decrypt(enc2) != data);
}


QTEST_MAIN(TestQrypto)
