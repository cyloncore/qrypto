/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractEncryptionAlgorithm.h"

#include "Engine_p.h"

using namespace Qrypto;

AbstractEncryptionAlgorithm::AbstractEncryptionAlgorithm()
{
  Private::Engine::init();
}

AbstractEncryptionAlgorithm::~AbstractEncryptionAlgorithm()
{

}

