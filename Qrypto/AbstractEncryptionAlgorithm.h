/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _QRYPTO_ABSTRACTENCRYPTIONALGORITHM_H_
#define _QRYPTO_ABSTRACTENCRYPTIONALGORITHM_H_

class QByteArray;

namespace Qrypto
{
  class AbstractEncryptionAlgorithm
  {
  protected:
    AbstractEncryptionAlgorithm();
    virtual ~AbstractEncryptionAlgorithm();
  public:
    virtual QByteArray encrypt(const QByteArray& _data) const = 0;
    virtual QByteArray decrypt(const QByteArray& _data) const = 0;
  };
}

#endif
