/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractEncryptionAlgorithm.h"

namespace Qrypto
{
  class BlowfishAlgorithm : public AbstractEncryptionAlgorithm
  {
  public:
    BlowfishAlgorithm();
    BlowfishAlgorithm(const QByteArray& _key);
    ~BlowfishAlgorithm();
    void setKey(const QByteArray& _key);
  public:
    virtual QByteArray decrypt(const QByteArray& _data) const;
    virtual QByteArray encrypt(const QByteArray& _data) const;
  private:
    struct Private;
    Private* const d;
  };
}
