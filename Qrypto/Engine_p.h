/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Qrypto {
  namespace Private {
    class Engine
    {
    private:
      Engine();
      ~Engine();
    public:
      /**
       * @brief initialise the OpenSSL library, if needed
       */
      static void init();
    private:
      struct Private;
      static Private s_private;
    };
  }
}
