/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _QRYPTO_RSAQLGORITHM_H_
#define _QRYPTO_RSAQLGORITHM_H_

#include "AbstractSigningAlgorithm.h"
#include "AbstractEncryptionAlgorithm.h"
#include "AbstractSerialisable.h"

namespace Qrypto
{
  /**
   * Interface to the RSA (Rivest–Shamir–Adleman) Algorithm.
   */
  class RSAAlgorithm : public AbstractSigningAlgorithm, public AbstractEncryptionAlgorithm, public AbstractSerialisable
  {
  public:
    enum {
      RSA_3 = 0x3L,
      RSA_F4 = 0x10001L
    };
  public:
    RSAAlgorithm();
    RSAAlgorithm(const RSAAlgorithm& _rhs);
    RSAAlgorithm& operator=(const RSAAlgorithm& _rhs);
    ~RSAAlgorithm();
    /**
     * Generate a new set of key
     */
    void generateKey(int _bits = 2048, int exponent = RSA_F4);
  public:
    QByteArray sign(const QByteArray& _data, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const override;
    bool validate(const QByteArray& _data, const QByteArray& _signature, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const override;
    QByteArray decrypt(const QByteArray& _data) const override;
    QByteArray encrypt(const QByteArray& _data) const override;
    QByteArray serialise(Key _key = Key::Best) const override;
    bool unserialise(const QByteArray& _data) override;
    bool load(const QString& _filename) override;
    /**
     * @return true if the key is valid
     */
    bool valid(Key _key = Key::Best) const;
  private:
    struct Private;
    Private* const d;
  };
}

#endif
