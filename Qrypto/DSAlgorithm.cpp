/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "DSAlgorithm.h"

 #include <cstdio>

#include <openssl/dsa.h>
#include <openssl/pem.h>

#include <QByteArray>
#include <QString>

using namespace Qrypto;

DSAlgorithm::DSAlgorithm() : m_payload(DSA_new())
{

}

DSAlgorithm::~DSAlgorithm()
{
  DSA_free(payload<DSA>());
}

void DSAlgorithm::generateKey(int _bits)
{
  DSA_generate_parameters_ex(payload<DSA>(), _bits, NULL, 0, NULL, NULL, NULL);
  DSA_generate_key(payload<DSA>());
  m_has_private_key = true;
}

QByteArray DSAlgorithm::sign(const QByteArray& _digest, Hash::Algorithm _algorithm) const
{
  if(not m_has_private_key) return QByteArray();
  QByteArray digest = Hash::compute(_algorithm, _digest);
  QByteArray ret; ret.resize(DSA_size(payload<DSA>()));
  unsigned int siglen = ret.length();
  
  DSA_sign(0 /* ignored */, (unsigned char*)digest.data(), digest.size(), (unsigned char*)ret.data(), &siglen, payload<DSA>());
  ret.resize(siglen);
  return ret;
}

bool DSAlgorithm::validate(const QByteArray& _digest, const QByteArray& _signature, Hash::Algorithm _algorithm) const
{
  QByteArray digest = Hash::compute(_algorithm, _digest);
  return DSA_verify(0 /* ignored */, (unsigned char*)digest.data(), digest.size(), (unsigned char*)const_cast<char*>(_signature.data()), _signature.size(), payload<DSA>()) == 1;
}

QByteArray DSAlgorithm::serialise(AbstractSerialisable::Key _key) const
{
  if(_key == AbstractSerialisable::Key::Best)
  {
    if(m_has_private_key)
    {
      _key = AbstractSerialisable::Key::Private;
    } else {
      _key = AbstractSerialisable::Key::Public;
    }
  }
  switch(_key)
  {
    case AbstractSerialisable::Key::Public:
    {
      int len = i2d_DSAPublicKey(payload<DSA>(), NULL);
      QByteArray result; result.resize(len);
      unsigned char *next = (unsigned char *)result.data();
      i2d_DSAPublicKey(payload<DSA>(), &next);
      return result;      
    }
    case AbstractSerialisable::Key::Private:
    {
      int len = i2d_DSAPrivateKey(payload<DSA>(), NULL);
      QByteArray result; result.resize(len);
      unsigned char *next = (unsigned char *)result.data();
      i2d_DSAPrivateKey(payload<DSA>(), &next);
      return result;      
    }
    default:
      qFatal("Unsupported key to serialise!");
  }
}

bool DSAlgorithm::unserialise(const QByteArray& _data)
{
  DSA_free(payload<DSA>());
  const unsigned char *next = (unsigned char *)_data.data();
  m_payload = d2i_DSAPrivateKey(NULL, &next, _data.size());
  if(not m_payload)
  {
    next = (unsigned char *)_data.data();
    m_payload = d2i_DSAPublicKey(NULL, &next, _data.size());
    m_has_private_key = false;
  } else {
    m_has_private_key = true;
  }
  return m_payload;
}

bool DSAlgorithm::load(const QString& _filename)
{
  DSA_free(payload<DSA>());
  FILE* fp = fopen(qPrintable(_filename), "r");
  m_payload = PEM_read_RSAPrivateKey(fp, nullptr, nullptr, const_cast<char*>(""));
  fclose(fp);
  if(m_payload)
  {
    m_has_private_key = true;
  } else {
    fp = fopen(qPrintable(_filename), "r");
    m_payload = PEM_read_RSAPublicKey(fp, nullptr, nullptr, const_cast<char*>(""));
    fclose(fp);
    m_has_private_key = false;
  }
  return m_payload;
}
