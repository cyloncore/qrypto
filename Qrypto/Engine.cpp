/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Engine_p.h"

#include <QDebug>

#include <openssl/rand.h>

using namespace Qrypto::Private;

struct Engine::Private
{
  Private() : initialised(false)
  {
    
  }
  bool initialised;
};

Engine::Private Engine::s_private;


void Engine::init()
{
  if(s_private.initialised) return;
  
  const int seed_bytes = 1024;
  
  if(!RAND_poll())
  {
    if (!RAND_load_file("/dev/urandom", seed_bytes))
    {
      if (!RAND_load_file("/dev/random", seed_bytes))
      {
        qFatal("Failed to seed OpenSSL random number!");
        return;
      }
    }
  }

  
  s_private.initialised = true;
}

