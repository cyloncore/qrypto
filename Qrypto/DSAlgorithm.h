/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractSigningAlgorithm.h"
#include "AbstractSerialisable.h"

namespace Qrypto
{
  class DSAlgorithm : public AbstractSigningAlgorithm, public AbstractSerialisable
  {
  public:
    DSAlgorithm();
    ~DSAlgorithm();
    void generateKey(int _bits = 1024);
  public:
    QByteArray sign(const QByteArray& _digest, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const override;
    bool validate(const QByteArray& _digest, const QByteArray& _signature, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const override;
    QByteArray serialise(Key _key = Key::Best) const override;
    bool unserialise(const QByteArray& _data) override;
    bool load(const QString& _filename) override;
  private:
    template<typename _T_> _T_* payload() const { return reinterpret_cast<_T_*>(const_cast<void*>(m_payload)); }
    void* m_payload;
    bool m_has_private_key = false;
  };
}
