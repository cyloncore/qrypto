/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "RSAAlgorithm.h"
#include "SplitedProcess_p.h"

#include <cstdio>

#include <openssl/objects.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

#include <QByteArray>
#include <QCryptographicHash>
#include <QString>

using namespace Qrypto;

struct RSAAlgorithm::Private
{
  Private() : payload(RSA_new()) {}
  RSA* payload;
  bool has_private_key  = false;
};

RSAAlgorithm::RSAAlgorithm() : d(new Private)
{

}

RSAAlgorithm::RSAAlgorithm(const RSAAlgorithm& _rhs) : d(new Private)
{
  unserialise(_rhs.serialise());
}

RSAAlgorithm& RSAAlgorithm::operator=(const RSAAlgorithm& _rhs)
{
  unserialise(_rhs.serialise());
  return *this;
}

RSAAlgorithm::~RSAAlgorithm()
{
  RSA_free(d->payload);
  delete d;
}

void RSAAlgorithm::generateKey(int _bits, int exponent)
{
  BIGNUM *bn = BN_new();
  BN_set_word(bn, exponent);
  RSA_generate_key_ex(d->payload, _bits, bn, NULL);
  d->has_private_key = true;
  BN_free(bn);
}

QByteArray RSAAlgorithm::sign(const QByteArray& _digest, Hash::Algorithm _algorithm) const
{
  QByteArray digest = Hash::compute(_algorithm, _digest);
  QByteArray ret; ret.resize(RSA_size(d->payload));
  unsigned int siglen = ret.length();
  
  RSA_sign(NID_sha1, (unsigned char*)digest.data(), digest.size(), (unsigned char*)ret.data(), &siglen, d->payload);
  ret.resize(siglen);
  return ret;
}

bool RSAAlgorithm::validate(const QByteArray& _digest, const QByteArray& _signature, Hash::Algorithm _algorithm) const
{
  QByteArray digest = Hash::compute(_algorithm, _digest);
  return RSA_verify(NID_sha1, (unsigned char*)digest.data(), digest.size(), (unsigned char*)const_cast<char*>(_signature.data()), _signature.size(), d->payload) == 1;
}

QByteArray RSAAlgorithm::decrypt(const QByteArray& _data) const
{
  RSA* rsa = d->payload;
  if(rsa and d->has_private_key)
  {
    const int chunk_size = RSA_size(rsa);
    return splitedProcess(_data, chunk_size, [rsa, chunk_size](uint8_t* ptr, std::size_t _size) -> QByteArray
      {
        QByteArray ret; ret.resize(RSA_size(rsa));
        int size = RSA_private_decrypt(_size, ptr, (unsigned char*)ret.data(), rsa, RSA_PKCS1_OAEP_PADDING);
        ret.resize(size);
        return ret;
      });
  } else {
    return QByteArray();    
  }
}

QByteArray RSAAlgorithm::encrypt(const QByteArray& _data) const
{
  RSA* rsa = d->payload;
  if(rsa)
  {
    const int chunk_size = RSA_size(rsa) - 42;
    return splitedProcess(_data, chunk_size, [rsa, chunk_size](uint8_t* ptr, std::size_t _size) -> QByteArray
      {
        QByteArray ret; ret.resize(RSA_size(rsa));
        int size = RSA_public_encrypt(_size, ptr, (unsigned char*)ret.data(), rsa, RSA_PKCS1_OAEP_PADDING);
        ret.resize(size);
        return ret;
      });
  } else {
    return QByteArray();
  }
}

QByteArray RSAAlgorithm::serialise(AbstractSerialisable::Key _key) const
{
  if(_key == AbstractSerialisable::Key::Best)
  {
    if(d->has_private_key)
    {
      _key = AbstractSerialisable::Key::Private;
    } else {
      _key = AbstractSerialisable::Key::Public;
    }
  }
  switch(_key)
  {
    case AbstractSerialisable::Key::Public:
    {
      int len = i2d_RSAPublicKey(d->payload, NULL);
      QByteArray result; result.resize(len);
      unsigned char *next = (unsigned char *)result.data();
      i2d_RSAPublicKey(d->payload, &next);
      return result;      
    }
    case AbstractSerialisable::Key::Private:
    {
      int len = i2d_RSAPrivateKey(d->payload, NULL);
      QByteArray result; result.resize(len);
      unsigned char *next = (unsigned char *)result.data();
      i2d_RSAPrivateKey(d->payload, &next);
      return result;      
    }
    default:
      qFatal("Unsupported key to serialise!");
  }
}

bool RSAAlgorithm::unserialise(const QByteArray& _data)
{
  RSA_free(d->payload);
  const unsigned char *next = (unsigned char *)_data.data();
  d->payload = d2i_RSAPrivateKey(NULL, &next, _data.size());
  if(not d->payload)
  {
    next = (unsigned char *)_data.data();
    d->payload = d2i_RSAPublicKey(NULL, &next, _data.size());
    d->has_private_key = false;
  } else {
    d->has_private_key = true;
  }
  return d->payload;
}

#include <openssl/err.h>
bool RSAAlgorithm::load(const QString& _filename)
{
  RSA_free(d->payload);
  FILE* fp = fopen(qPrintable(_filename), "r");
  d->payload = PEM_read_RSAPrivateKey(fp, nullptr, nullptr, nullptr);
  ERR_print_errors_fp(stderr);
  fclose(fp);
  if(d->payload)
  {
    d->has_private_key = true;
  } else {
    fp = fopen(qPrintable(_filename), "r");
    d->payload = PEM_read_RSAPublicKey(fp, nullptr, nullptr, nullptr);
    fclose(fp);
    d->has_private_key = false;
  }
  return d->payload;
}

bool RSAAlgorithm::valid(AbstractSerialisable::Key _key) const
{
  switch(_key)
  {
    case Key::Best:
      return valid(Key::Public);
    case Key::Private:
      return d->has_private_key and d->payload;
    case Key::Public:
      return d->payload;
  }
  return false;
}
