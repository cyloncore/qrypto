/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _QRYPTO_ABSTRACTSERIALISABLE_H_
#define _QRYPTO_ABSTRACTSERIALISABLE_H_

class QByteArray;
class QString;

namespace Qrypto
{
  class AbstractSerialisable
  {
  protected:
    virtual ~AbstractSerialisable();
  public:
    enum class Key {
      Public,   //< serialise the public key
      Private,  //< serialise the private key
      Best      //< serialise the best key (private if available otherwise public)
    };
    virtual QByteArray serialise(Key _key = Key::Best) const = 0;
    virtual bool unserialise(const QByteArray& _data) = 0;
    virtual bool load(const QString& _filename) = 0;
  };
}

#endif
