/*
 *  Copyright (c) 2014,2020 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef HASH_H
#define HASH_H

#include <QCryptographicHash>
#include <QString>

namespace Qrypto
{
  namespace Hash
  {
    enum class Algorithm {
      RAW,
      MD5,
      SHA1,
      SHA2,
      SHA3
    };
    static inline QByteArray compute(Algorithm _algorithm, const QByteArray& _array)
    {
      switch(_algorithm)
      {
      case Algorithm::RAW:
        return _array;
      case Algorithm::MD5:
        return QCryptographicHash::hash(_array, QCryptographicHash::Md5);
      case Algorithm::SHA1:
        return QCryptographicHash::hash(_array, QCryptographicHash::Sha1);
      case Algorithm::SHA2:
        return QCryptographicHash::hash(_array, QCryptographicHash::Sha512);
      case Algorithm::SHA3:
        return QCryptographicHash::hash(_array, QCryptographicHash::Sha3_512);
      }
      qFatal("Unsupported hash algorithm");
    }
    template<typename _T_, typename _Enable_ = void>
    struct Compute;
    template<>
    struct Compute<QByteArray>
    {
      Compute(QCryptographicHash& _algorithm, const QByteArray& _v)
      {
        _algorithm.addData(_v);
      }
    };
    template<>
    struct Compute<QString>
    {
      Compute(QCryptographicHash& _algorithm, const QString& _v)
      {
        _algorithm.addData(_v.toUtf8());
      }
    };
    template<typename _T_>
    struct Compute<_T_, std::enable_if_t<std::is_trivial_v<_T_>>>
    {
      Compute(QCryptographicHash& _algorithm, const _T_& _v)
      {
        _algorithm.addData(reinterpret_cast<const char*>(&_v), sizeof(_T_));
      }
    };    
    template<typename _T_>
    inline void compute(QCryptographicHash& _algorithm, const _T_& _v)
    {
      Compute<_T_>(_algorithm, _v);
    }
    template<typename _T_, typename _T2_, typename... _TOther_>
    inline void compute(QCryptographicHash& _algorithm, const _T_& _v, const _T2_& _v2, const _TOther_&... _values)
    {
      compute(_algorithm, _v);
      compute<_T2_, _TOther_...>(_algorithm, _v2, _values...);
    }

    template<typename... _T_>
    inline QByteArray compute(Algorithm _algorithm, const _T_&... _values)
    {
      QCryptographicHash::Algorithm algo;
      switch(_algorithm)
      {
      case Algorithm::RAW:
        qFatal("Cannot be RAW");
      case Algorithm::MD5:
        algo = QCryptographicHash::Md5;
        break;
      case Algorithm::SHA1:
        algo = QCryptographicHash::Sha1;
        break;
      case Algorithm::SHA2:
        algo = QCryptographicHash::Sha512;
        break;
      case Algorithm::SHA3:
        algo = QCryptographicHash::Sha3_512;
        break;
      }
      QCryptographicHash hash(algo);
      compute(hash, _values...);
      return hash.result();
    }
    
#define __QRYPTO_ALGORITHM(_NAME_, _ENUM_)                    \
    template<typename... _T_>                                 \
    static inline QByteArray _NAME_(const _T_&... _values)    \
    {                                                         \
      return compute<_T_...>(Algorithm::_ENUM_, _values...);  \
    }
    __QRYPTO_ALGORITHM(md5, MD5)
    __QRYPTO_ALGORITHM(sha1, SHA1)
    __QRYPTO_ALGORITHM(sha2, SHA2)
    __QRYPTO_ALGORITHM(sha3, SHA3)
  };
}

#endif // HASH_H
