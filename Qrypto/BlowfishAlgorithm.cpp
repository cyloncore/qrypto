/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "BlowfishAlgorithm.h"
#include "SplitedProcess_p.h"

#include <cstring>

#include <QByteArray>

#include <openssl/blowfish.h>

using namespace Qrypto;

struct BlowfishAlgorithm::Private
{
  BF_KEY key;
};

BlowfishAlgorithm::BlowfishAlgorithm() : d(new Private)
{

}

BlowfishAlgorithm::BlowfishAlgorithm(const QByteArray& _key) : d(new Private)
{
  setKey(_key);
}

BlowfishAlgorithm::~BlowfishAlgorithm()
{
  delete d;
}

void BlowfishAlgorithm::setKey(const QByteArray& _key)
{
  BF_set_key(&d->key, _key.size(), (uint8_t*)_key.data());
}

QByteArray BlowfishAlgorithm::decrypt(const QByteArray& _data) const
{
  uint8_t ivec[8];
  std::memset(ivec, 0, 8 * sizeof(char));
  int ivec_pos = 0;
  
  return splitedProcess(_data, 64, [&ivec, &ivec_pos, this](uint8_t* ptr, std::size_t _size) -> QByteArray
  {
    QByteArray ret; ret.resize(64);
    BF_cfb64_encrypt(ptr, (uint8_t*)ret.data(), _size, &d->key, ivec, &ivec_pos, 0);
    ret.resize(_size);
    return ret;
  });
}

QByteArray BlowfishAlgorithm::encrypt(const QByteArray& _data) const
{
  uint8_t ivec[8];
  std::memset(ivec, 0, 8 * sizeof(char));
  int ivec_pos = 0;
  
  return splitedProcess(_data, 64, [&ivec, &ivec_pos, this](uint8_t* ptr, std::size_t _size) -> QByteArray
  {
    QByteArray ret; ret.resize(64);
    BF_cfb64_encrypt(ptr, (uint8_t*)ret.data(), _size, &d->key, ivec, &ivec_pos, 1);
    ret.resize(_size);
    return ret;
  });
}
