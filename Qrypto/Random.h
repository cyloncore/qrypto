/*
 *  Copyright (c) 2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QByteArray>

namespace Qrypto
{
  class Random
  {
  public:
    static QByteArray generate(int _size);
    static bool generate(QByteArray* _data);
  };
}
