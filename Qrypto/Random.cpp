/*
 *  Copyright (c) 2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Random.h"

#include <openssl/rand.h>

#include <QByteArray>

using namespace Qrypto;

QByteArray Random::generate(int _size)
{
  QByteArray arr;
  arr.resize(_size);
  generate(&arr);
  return arr;
}

bool Random::generate(QByteArray* _data)
{
  return RAND_bytes((unsigned char*)_data->data(), _data->size()) == 1;
}
