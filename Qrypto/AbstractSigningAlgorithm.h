/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _QRYPTO_ABSTRACTSIGNINGALGORITHM_H_
#define _QRYPTO_ABSTRACTSIGNINGALGORITHM_H_
#include "Hash.h"

class QByteArray;

namespace Qrypto
{
  class AbstractSigningAlgorithm
  {
  protected:
    AbstractSigningAlgorithm();
    virtual ~AbstractSigningAlgorithm();
  public:
    virtual QByteArray sign(const QByteArray& _digest, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const = 0;
    virtual bool validate(const QByteArray& _digest, const QByteArray& _signature, Hash::Algorithm _algorithm = Hash::Algorithm::SHA2) const = 0;
  };
}

#endif
