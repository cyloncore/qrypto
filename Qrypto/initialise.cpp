#include "initialise.h"

# include <openssl/crypto.h>

namespace Qrypto
{
  void initialise()
  {
    OPENSSL_init_crypto(OPENSSL_INIT_ADD_ALL_DIGESTS | OPENSSL_INIT_ADD_ALL_CIPHERS, nullptr);
  }
}
