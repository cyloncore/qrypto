/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <functional>
#include <QByteArray>

namespace
{
  QByteArray splitedProcess(const QByteArray& _source, int _chunkSize, const std::function<QByteArray (uint8_t*, std::size_t)>& _function)
  {
    QByteArray result;
    int idx = 0;
    while(idx + _chunkSize < _source.size())
    {
      result.append(_function((uint8_t*)_source.data() + idx, _chunkSize));
      idx += _chunkSize;
    }
    result.append(_function((uint8_t*)_source.data() + idx, _source.size() - idx));
    return result;
  }
}
